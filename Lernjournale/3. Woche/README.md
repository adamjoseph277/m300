# Lernjournal - Woche 3 - 17.5.2024

## **Einleitung**

## **Wochenzieleagesziele**
Für diese Woche war mein Ziel das ich eine Flask App habe die ich Lokal betreibe. Ausserdem wollte ich das Datenbank Modell welches unten ist machen.

## **Umsetzung**

Ich wollte die Flask App und die Datenbank noch nicht auf AWS erstellen weil ich das Guthaben nicht aufbrauchen wollte, deshalb habe ich lokal mit Doker Desktop einen Datenbank Container erstellt. Ausserdem habe ich eine simple Flask Applikation aufgebaut und die Datenbank dann Integriert.

Für die Datenbank habe ich dann ein Initielles Datenbankmodell erstellt:
![SQL](./img/sql.png)
Hier sieht man nicht vieles was AWS speziefisch ist, das liegt daran, das ich zuerst die Basics haben will bevor ich AWS integriere.

Ich habe das Login mit dem Python Modul "Flask-Login" gelöst, damit ich nicht selber die Sessions programmieren muss, ausserdem habe ich noch die Möglichkeit gemacht, Karten zu erstellen damit. eine Karte soll dann später ein Minecraftserver sein. jeder User kann dann auch nur seine eigenen Karten sehen oder besser gesagt die Karten von welche er der Owner ist:
![Cards](./img/cards.png)
Und die Sidebar ist mit Placeholder ovn Chat GPT gefüllt worden.
Die Knöpfe "Go To VM" und "Edit Course" sind Placeholder für später

## **Probleme**
Probleme gab es diese Woche gross keine, weil ich mich schon mit Flask auskenne. Es lief alles Rund.