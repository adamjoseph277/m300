# Lernjournal - Woche 1 - 17.5.2024

## **Einleitung**

Heute war der Einstieg des Moduls. In diesem Modul geht es darum ein Projekt zu machen welches mit der Cloud im zusammenhang ist.

## **Wochenzieleagesziele**

- Git-Repo wird für das Modul aufgesetzt.
- Modulziele werden nachgeschaut
- Projektidee wird formuliert
- evt. Planung des Projekts

## **Umsetzung**

### Git-Repo
Ein Gitrepository ist aufgesetzt, ich kann aber noch nicht sagen wie die Struktur sein wird. Das Repo wird on-the-go gepflegt.

Link für das Repo: https://gitlab.com/adamjoseph277/m300

### Modulziele

Die Modulziele kann man im Modulbaukasten der ICT-Berufsbildung nachschauen unter: https://www.modulbaukasten.ch/module/300/4/de-DE?title=Plattform%C3%BCbergreifende-Dienste-in-ein-Netzwerk-integrieren

### Projektidee

Ich möchte mein Projekt auf der AWS Cloud umsetzen. Ich möchte einen Klon von Aternos machen. 

- AWS Loadbalancer
- AWS Autoscaling
- VPC
- ECS
- EC2
- RDS
- S3
- VPN

Andere User zugriff auf die Server geben.
Console

Sicherheit:
Firewall 
RDS nur über Webserver erreichbar und oder VPN

