# Lernjournal - Woche 7-9


## **Backups**

### **Wochenziele**
- Einrichtung von automatisierten Backups für die Datenbank und wichtige Dateien.
- Testen der Wiederherstellung von Backups.
- Dokumentation der Backup-Strategie.

### **Umsetzung**

#### Backup-Strategie
Zunächst habe ich eine Backup-Strategie formuliert. Die wichtigsten Punkte sind:
- Tägliche Backups der Datenbank in AWS RDS.
- Wöchentliche Backups der Anwendungsdaten in AWS S3.
- Monatliche Snapshots der gesamten EC2-Instanzen.

#### Automatisierte Backups
1. **RDS-Backups**:
   - RDS-Instanz so konfiguriert, dass sie tägliche automatische Backups erstellt.
   - Manuelles Backup erstellt, um sicherzustellen, dass die Konfiguration korrekt ist.

2. **S3-Backups**:
   - AWS CLI-Skript geschrieben, das wichtige Dateien täglich in einen S3-Bucket hochlädt.
   - Das Skript in eine cron job integriert, um die tägliche Ausführung zu automatisieren.

3. **EC2-Snapshots**:
   - AWS Lambda-Funktion geschrieben, die monatlich Snapshots der EC2-Instanzen erstellt.
   - CloudWatch-Event erstellt, um die Lambda-Funktion monatlich auszuführen.

#### Probleme und Lösungen
- **Problem**: Initial schlug die Backup-Konfiguration fehl, weil die IAM-Rollen nicht korrekt gesetzt waren.
  - **Lösung**: Die IAM-Rollen überarbeitet und die nötigen Berechtigungen hinzugefügt.


---

## **Whitelisting**

### **Wochenziele**
- Implementierung eines Whitelisting-Mechanismus für den Zugang zu den Servern.
- Testen des Whitelisting-Prozesses.
- Dokumentation der Whitelisting-Strategie.

### **Umsetzung**

#### Whitelisting-Strategie
Die Whitelisting-Strategie umfasst:
- Beschränkung des Zugangs zu den EC2-Instanzen basierend auf IP-Adressen.
- Nutzung von AWS Security Groups und Network ACLs zur Durchsetzung des Whitelists.

#### Implementierung
1. **Security Groups**:
   - Security Group für die EC2-Instanzen erstellt und konfiguriert.
   - Regel hinzugefügt, die nur spezifische IP-Adressen erlaubt.

2. **Network ACLs**:
   - Zusätzliche Sicherheitsschicht durch Network ACLs auf Subnetzebene implementiert.
   - Nur Verkehr von Whitelisted-IP-Adressen zugelassen.

#### Probleme und Lösungen
- **Problem**: Initial funktionierte der Zugriff nicht, weil die IP-Adressen falsch formatiert waren.
  - **Lösung**: Die IP-Adressformate überprüft und korrekt konfiguriert.


#### Testen des Whitelists
- Verbindungen von erlaubten und nicht erlaubten IP-Adressen getestet, um sicherzustellen, dass das Whitelisting korrekt funktioniert.

---

## **Erstellung der Dokumentation**

### **Wochenziele**
- Vollständige Dokumentation des Projekts erstellen.
- Sicherstellen, dass alle wichtigen Schritte und Konfigurationen abgedeckt sind.
- Dokumentation im Git-Repo veröffentlichen.

### **Umsetzung**

#### Dokumentationsstrategie
- Die Dokumentation wurde in Markdown verfasst, um sie einfach im Git-Repo zu pflegen.
- Folgende Abschnitte wurden abgedeckt:
  - Projektübersicht
  - Setup-Anleitungen
  - Backup-Strategie
  - Whitelisting-Strategie
  - Sicherheit
  - Problembehandlung

---

## **Fazit**
Diese Wocheen war sehr produktiv und ich konnte wichtige Meilensteine in Bezug auf Backups, Whitelisting und Dokumentation erreichen. Durch die Lösungen der auftretenden Probleme habe ich viel gelernt und konnte die Sicherheit und Zuverlässigkeit meines Projekts verbessern.
