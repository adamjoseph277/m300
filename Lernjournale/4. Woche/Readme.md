# Lernjournal - Woche 4 - 17.5.2024

## **Einleitung**
Diese Woche hatte ich ab und zu wirklich Probleme, weil ich mich nicht so gut mit dem AWS-CLI auskannte.

## **Wochenziele**
Für diese Woche war das Ziel, das, was ich schon habe, mit AWS zu verbinden.

## **Umsetzung**

### AWS CLI
Ich habe in dieser Woche zuerst mal mit dem AWS CLI experimentiert. Ich musste das CLI herunterladen und habe die Credentials vom Learner Lab in das File eingefügt und etwas mit diesen experimentiert.

### Boto3
Damit ich das AWS CLI mit Python benutzen kann, gibt es ein Python-Modul mit dem Namen "Boto3". Ich habe dieses installiert und etwas damit experimentiert, indem ich Ressourcen von AWS ausgelesen habe.

Ich habe mich zuerst informiert, wie die AWS-Infrastruktur mit Containern umgeht. Dies macht es mit Clustern, Task-Definitionen, Services und Tasks.

Ich schrieb danach ein Skript, damit ich Task-Definitions erstellen kann für die Minecraft-Container. Danach habe ich ein Cluster erstellt und diese getestet. Danach probierte ich etwas mit Services herum, bis es für mich stimmte, und schrieb dies auch noch in ein Skript, damit es für einen Minecraft-Server erstellt.

Hier habe ich auch realisiert, dass ich noch Volumes brauche für die Server und habe mich entschieden, dies mit EFS zu lösen. Nachdem eine Task-Definition erstellt wird, erstellt eine Lambda-Funktion ein Verzeichnis in einem EFS und mountet dann ein Volume für den Service.

Danach hat es geklappt, einen Server zu erstellen. Das Einzige, was fehlt, ist, dass der User noch keine IP sieht. Ich habe dann die Flask-App upgedatet. Wenn man auf den Knopf "Go To VM" klickt, sieht man nun die IP-Adresse und den Status der Maschine. Außerdem habe ich noch ein paar Platzhalter hinzugefügt.
![Info](./img/Info.png)

Wie man auf dem Screenshot auch noch sieht habe ich noch Routen hinzugefügt um den Container zu starten und stoppen.

## **Probleme**

### Credential-Änderung vom Learner Lab
**Problem:**
Dieses Problem war das mühsamste, welches ich bis zu diesem Zeitpunkt hatte. Ich hatte ein Skript, welches mir die Task-Definitions von ECS ausliest. Dies funktionierte auch einwandfrei. Ein paar Stunden später, als ich dies testen wollte, bekam ich einen "Access Denied"-Fehler und ich wusste nicht warum.

**Troubleshooting:** Ich suchte die ganze Zeit im Internet herum, was es sein könnte, kam aber ewig auf keine Lösung. Ich dachte, es hatte etwas mit meinen Berechtigungen zu tun, weil diese vom Learner Lab gemanaged werden. Aber im Readme vom Learner Lab heißt es, dass ich diese Berechtigungen haben sollte. Auf der Konsole funktionierte es auch, deshalb war ich verwirrt.

**Lösung:** Am Schluss, nach 3 Stunden, stellte sich heraus, dass das Learner Lab seine AWS-Credentials jedes Mal ändert, wenn man es stoppt und startet und deshalb die Berechtigungen weg waren. Man musste die Credentials einfach updaten und es funktionierte.

### EFS-Share nicht auf den Root

**Problem:** Mit dem EFS-Share hatte ich auch ziemliche Probleme. Immer wenn ich einen Service erstellen wollte, mit dem Container Volume gemapped zum EFS-Share, bekam ich einen "Access Denied"-Fehler und ich wusste nicht warum dies so war.

**Troubleshooting:** Ich suchte überall herum, fand viele, welche den gleichen Fehler hatten, aber ihre Lösungen brachten nichts. Ich habe auch noch eine EC2-Instanz erstellt und mit dieser etwas herumexperimentiert, indem ich das EFS gemountet habe, aber es half nichts.

**Lösung:** Im Container muss man einen Access Point angeben, der auf das EFS-Share zeigt. Ich erstellte den Access Point auf die Route "/<name-Container>". Und es stellte sich heraus, dass weil dieses Verzeichnis noch nicht existierte, konnte ich es nicht referenzieren. Meine Lösung war es dann, eine Lambda-Funktion zu machen, welche dann ein Verzeichnis im EFS erstellt, bevor ich es referenziere.

### Lambda keine Berechtigung auf EFS

**Problem:** Ich hatte noch das Problem, dass AWS Lambda keine Berechtigung hatte, um ein Verzeichnis zu erstellen.

**Troubleshooting:** Hier dachte ich wieder, dass vielleicht die Rolle die Berechtigung nicht hatte und ich versuchte, die Policies zu erstellen, welche Lambda Rechte geben, um das EFS-Share zu bearbeiten. Dies funktionierte aber nicht.

**Lösung:** Die Lösung war dann eher simpel: ich mounte den Share einfach mit vollen Berechtigungen. Dies ist aber nur eine temporäre Lösung und ich schaue, wie ich es in der Zukunft mache, weil dies ist eine Sicherheitslücke.
