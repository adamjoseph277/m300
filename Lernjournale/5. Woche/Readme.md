# Lernjournal - Woche 5 - 14.6.2024

## **Einleitung**
Heute habe ich mir als Ziel gesetzt, das ich diese Woche ein bisschen mit den Lernjournalen aufhole, weil ich habe diese ein bisschen vernachläsigt.

## **Wochenzieleagesziele**
Wie in der Einleitung gesagt, will ich die Lernjourale aufholen, ausserdem will ich mich einbisschen Informieren wie ich die VPN verbindung machen kann Und die EC2 Instanzen aufbauen kann.

## **Umsetzung**

Wie in den Zielen erwähnt, habe ich diese Woche vorallem an den Lerndokumentationen und Systemdokumentationen gearbeitet.

Produktiv habe ich aber auch ein paar sachen hinbekommen: Ich habe eine EC2 Instanz aufgesetzt als Vorbereitung für das deployment der Flaskapp.
Ausserdem habe ich auch die Whiteliste implemenation gemacht mit RCON. dazu benutzte ich die [Dokumentation](https://docker-minecraft-server.readthedocs.io/en/latest/configuration/server-properties/#whitelist-players) 
vom Container Image von Minecraft. ich musste aber noch ein paar sachen am vorhanden Code anpaassen und ausserdem habe ich 2 neue Routen für die WHitelist hinzugefügt.

## **Probleme**

Probleme hatte ich Grundsätzlich nicht diese Woche. trotzdem habe ich mir noch ein paar sachen auf meine TODO liste gestellt die mir aufgefallen sind.
z.B. Ein Mechanismus, welcher checkt ob der Minecraftserver schon online ist bevor er diesen als Aktiv anzeigt. Immoment zeigt es schon aktiv an wenn der Container läuft.

Ich habe mich auch dazu entschieden, einen Service zu erstellen welcher mir die Minecraftserver auf domain Namen mapped, damit man diese mit einem Domain Namen erreichen kann.
dieses Konzept muss ich mir aber noch ein bisschen anschauen weil dies etwas komplizierter sein könnte.