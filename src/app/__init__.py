from os import getenv, environ
from flask import Flask
from dotenv import load_dotenv
from .views import views
from flask_login import LoginManager
from .models import db


load_dotenv()

app = Flask(__name__)
app.secret_key = getenv('FLASK_SECRET_KEY')
app.register_blueprint(views, url_prefix="/")

app.config["SQLALCHEMY_DATABASE_URI"] = 'mysql+pymysql://{MYSQL_USER}:{MYSQL_PASSWORD}@{MYSQL_HOST}:{MYSQL_PORT}/{MYSQL_DATABASE}'.format(
    **dict(environ.items()))

db.init_app(app)
login_manager = LoginManager(app)
login_manager.login_view = '/login'

def create_app():
    with app.app_context():
        db.create_all()
    return app


@login_manager.user_loader
def load_user(name_user):
    user = models.Users.query.get(name_user)
    return user