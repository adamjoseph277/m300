# views.py
from flask import Blueprint, render_template, request, redirect, flash, url_for
from dotenv import load_dotenv
from flask_login import login_user, current_user, logout_user, login_required
from .models import Users, db, User_has_Mcsrv, Mcsrvs
from .AWS import AWS
import bcrypt
import json
import os
import random
import string

load_dotenv()

views = Blueprint('views', __name__)
aws = AWS(os.environ.get("RCON_PASSWORD"))


@views.route("/")
@login_required
def home():
    owned_srv = db.session.query(
        Mcsrvs.name_mcsrv.label('name'),
        Mcsrvs.id_mcsrv.label('id'),
        User_has_Mcsrv.name_user.label('name_user')
    ).join(
        User_has_Mcsrv, User_has_Mcsrv.id_mcsrv == Mcsrvs.id_mcsrv
    ).join(
        Users, User_has_Mcsrv.name_user == Users.name_user
    ).filter(
        User_has_Mcsrv.id_role == 1,
        Users.name_user == current_user.name_user
    ).all()

    member_srv = db.session.query(
        Mcsrvs.name_mcsrv.label('name'),
        Mcsrvs.id_mcsrv.label('id')
    ).join(
        User_has_Mcsrv, User_has_Mcsrv.id_mcsrv == Mcsrvs.id_mcsrv
    ).filter(
        User_has_Mcsrv.name_user == current_user.name_user,
        User_has_Mcsrv.id_role == 2
    ).all()

    return render_template('home.html', owned_srv=owned_srv, member_srv=member_srv)


@views.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect('/')

    if request.method == 'POST':
        username = request.form['username']
        email = request.form['email']
        password = request.form['password']

        if Users.query.filter_by(name_user=username).first():
            flash("This username is already used", "danger")
            return redirect('/register')
        elif Users.query.filter_by(email_user=email).first():
            flash("This email is already used", "danger")
            return redirect('/register')

        # Hash the password using bcrypt
        hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())

        # Create new user
        new_user = Users(name_user=username, email_user=email, hash_user=hashed_password)
        db.session.add(new_user)
        db.session.commit()

        login_user(new_user)

        return redirect('/')

    return render_template('register.html')


@views.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect('/')

    if request.method == 'POST':
        username = request.form["username"]
        password = request.form["password"]

        user = Users.query.filter_by(name_user=username).first()
        if not user:
            print("no user")
            return render_template('login.html')
        if bcrypt.checkpw(password.encode('utf-8'), user.hash_user.encode('utf-8')):
            login_user(user)
        else:
            print("wrong password")
            return render_template('login.html')
    return render_template('login.html')


@views.route('/logout')
def logout():
    logout_user()
    return redirect('/login')


@views.route('/create_server', methods=["GET", "POST"])
@login_required
def create_server():
    if request.method == 'POST':
        name_srv = request.form['namesrv']

        name = current_user.name_user + '-' + name_srv
        service_name = 'service' + '-' + name_srv
        arn_srv = aws.register_task_definition(name, name_srv)
        service_arn = None
        if arn_srv:
            print("created Task defintion")
            response = aws.invoke_Create_EFS(name_srv)
            if response == 201 or response == 200:
                print("Volume created or it already existed")
                service_arn = aws.create_service(service_name, arn_srv)
                print(service_arn)
            else:
                print("Opfer1")
        else:
            print("Opfer")

        mcsrv = Mcsrvs(arn_taskdef_mcsrv=arn_srv, arn_service_mcsrv=service_arn, name_mcsrv=name_srv)
        db.session.add(mcsrv)
        db.session.commit()

        mcsrv = Mcsrvs.query.filter_by(arn_taskdef_mcsrv=arn_srv).first()
        if mcsrv:
            u_h_m = User_has_Mcsrv(name_user=current_user.name_user, id_mcsrv=mcsrv.id_mcsrv, id_role=1)
            db.session.add(u_h_m)
            db.session.commit()
            return redirect('/')
        else:
            print("something went wrong")

    return render_template('create_server.html')


@views.route('/edit_server/<server_id>', methods=["GET", "POST"])
@login_required
def edit_server(server_id):
    mcsrv = Mcsrvs.query.filter_by(id_mcsrv=server_id).first()
    status, ip, hostname= aws.server_info(mcsrv.arn_service_mcsrv)

    if not ip:
        flash("Server is stopped")
        return redirect('/')

    if request.method == 'POST':
        mcsrv.name_mcsrv = request.form['namesrv']
        mcsrv.whitelist = json.dumps(request.form.getlist('whitelist'))
        db.session.commit()
        return redirect('/')
    whitelist = aws.whitelist_list(ip)

    split_by_colon = whitelist.split(':')
    if len(split_by_colon) == 2:
        usernames_part = split_by_colon[1].strip()
        whitelist = [user.strip() for user in usernames_part.split(',')]
    else:
        whitelist = []
    return render_template('edit_server.html', name_server=mcsrv.name_mcsrv, id_server=server_id, whitelist=whitelist)


@views.route('/server/<server>')
@login_required
def server(server):
    mcsrv = Mcsrvs.query.filter_by(id_mcsrv=server).first()
    status, ip, server_address = aws.server_info(mcsrv.arn_service_mcsrv)
    print(mcsrv.arn_service_mcsrv)
    print(ip)
    return render_template("server_info.html", server=server, status=status, ip_address=ip, server_address=server_address)


@views.route('/server/<server>/stop', methods=["POST"])
@login_required
def stop_server(server):
    mcsrv = Mcsrvs.query.filter_by(id_mcsrv=server).first()
    if aws.stop_server(mcsrv.arn_service_mcsrv, 0):
        print("Stopping")
    else:
        print("not Stopping")

    return redirect('/')


@views.route('/server/<server>/start', methods=["POST"])
@login_required
def start_server(server):
    mcsrv = Mcsrvs.query.filter_by(id_mcsrv=server).first()
    if aws.stop_server(mcsrv.arn_service_mcsrv, 1):
        print("Starting")
    else:
        print("not Starting")

    return redirect('/')


@views.route('/server/<int:server_id>/whitelist/add', methods=['POST'])
def add_to_whitelist(server_id):
    usernames = request.form.get('usernames').split(',')
    server = Mcsrvs.query.get(server_id)
    status, public_ip, server_adress = aws.server_info(server.arn_service_mcsrv)

    if status == "RUNNING" and public_ip:
        print(public_ip, usernames)
        aws.whitelist_add(public_ip, usernames)
        return redirect(url_for('views.edit_server', server_id=server_id))
    else:
        return "Server not running", 400


@views.route('/server/<int:server_id>/whitelist/remove', methods=['POST'])
def remove_from_whitelist(server_id):
    usernames = request.form.get('usernames').split(',')
    server = Mcsrvs.query.get(server_id)
    status, public_ip, server_address = aws.server_info(server.arn_service_mcsrv)

    if status == "RUNNING" and public_ip:
        aws.whitelist_remove(public_ip, usernames)
        return redirect(url_for('views.edit_server', server_id=server_id))
    else:
        return "Server not running", 400


@views.route('/server/<server_id>/backup', methods=['POST'])
def backup(server_id):
    try:
        server_details = Mcsrvs.query.filter_by(id_mcsrv=server_id).first()

        if server_details:
            server_name = server_details.name_mcsrv
            s3_directory_id = ''.join(random.choices(string.ascii_lowercase + string.digits, k=10))

            efs_path = f"/mnt/{server_name}"

            s3_path = f"backups/{s3_directory_id}"

            success = aws.upload_files(efs_path, s3_path)

            if not success:
                print(success)
                return "Failed to upload files to S3", 500

            # Add entry to the database for this backup
            try:
                # Create a new entry in Backups table
                new_backup = Backups(
                    path_s3=s3_path,
                    id_mcsrv=server_id  # Assuming id_mcsrv is the foreign key reference
                )

                db.session.add(new_backup)
                db.session.commit()

                return f"Backup for server ID {server_id} ({server_name}) completed and saved in S3 at {s3_path}", 200

            except Exception as e:
                db.session.rollback()
                return f"Failed to create backup entry in database: {str(e)}", 500

        else:
            return f"Server with ID {server_id} not found in database", 404

    except Exception as e:
        return f"Error fetching server details: {str(e)}", 500
@views.route('/server/<server>/restore_backup')
def restore_backup(server):
    return "nice"
