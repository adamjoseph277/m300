import random
import os
import requests
from boto3 import client
import json
from rcon.source import Client
from mcrcon import MCRcon, MCRconException
from botocore.exceptions import NoCredentialsError
from datetime import datetime


class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        return super(DateTimeEncoder, self).default(obj)


class AWS:
    def __init__(self, rcon_password):
        self.ECS_client = client("ecs", region_name='us-east-1')
        self.EC2_client = client("ec2", region_name='us-east-1')
        self.Lambda_client = client("lambda", region_name='us-east-1')
        self.s3_client = client('s3')
        self.s3_bucket = 'mc-backups--m300'
        self.cluster = 'arn:aws:ecs:us-east-1:744663755802:cluster/McCluster'
        self.arn_mcRouter = 'arn:aws:ecs:us-east-1:744663755802:service/McCluster/service-mcrouter'
        self.recon_password = rcon_password

        self.domain_names = [
            "java.pixelcube.ch",
            "sumatra.pixelcube.ch",
            "borneo.pixelcube.ch",
            "sulawesi.pixelcube.ch",
            "luzon.pixelcube.ch",
            "taiwan.pixelcube.ch",
            "honshu.pixelcube.ch",
            "hainan.pixelcube.ch",
            "cuba.pixelcube.ch",
            "madagascar.pixelcube.ch",
            "sicily.pixelcube.ch",
            "sriLanka.pixelcube.ch",
            "cyprus.pixelcube.ch",
            "crete.pixelcube.ch",
            "corsica.pixelcube.ch",
            "iceland.pixelcube.ch",
            "newfoundland.pixelcube.ch",
            "tasmania.pixelcube.ch",
            "mauritius.pixelcube.ch",
            "malta.pixelcube.ch"
        ]

    def register_task_definition(self, name, name_srv):
        response = self.register_task_defintion_json(name, name_srv)
        return response.get('taskDefinition', {}).get('taskDefinitionArn', None)

    def register_task_defintion_json(self, name, name_srv):
        return self.ECS_client.register_task_definition(
            family=name,
            networkMode='awsvpc',
            taskRoleArn='arn:aws:iam::744663755802:role/LabRole',
            executionRoleArn='arn:aws:iam::744663755802:role/LabRole',
            containerDefinitions=[
                {
                    'name': 'test_mc',
                    'image': 'itzg/minecraft-server',
                    'portMappings': [
                        {
                            'containerPort': 25565,
                            'hostPort': 25565,
                            'protocol': 'tcp'
                        },
                        {
                            'containerPort': 25575,
                            'hostPort': 25575,
                            'protocol': 'tcp'
                        }
                    ],
                    'environment': [
                        {
                            'name': 'EULA',
                            'value': 'TRUE'
                        },
                        {
                            'name': 'ENABLE_WHITELIST',
                            'value': 'TRUE'
                        },
                        {
                            'name': 'RCON_PASSWORD',
                            'value': self.recon_password
                        }
                    ],
                    "mountPoints": [
                        {
                            "sourceVolume": "data-vol",
                            "containerPath": "/data",
                            "readOnly": False
                        }
                    ],
                    "logConfiguration": {
                        "logDriver": "awslogs",
                        "options": {
                            "awslogs-group": "/ecs/testtask",
                            "awslogs-create-group": "true",
                            "awslogs-region": "us-east-1",
                            "awslogs-stream-prefix": "ecs"
                        },
                        "secretOptions": []
                    }
                }
            ],
            volumes=[
                {
                    "name": "data-vol",
                    "efsVolumeConfiguration": {
                        "fileSystemId": "fs-00bf09c9cdf51e73a",
                        "rootDirectory": "/" + name_srv,
                        "transitEncryptionPort": 2049,
                        "transitEncryption": "ENABLED",
                    }
                }
            ],
            tags=[
                {
                    'key': 'Owner',
                    'value': 'Onkeljoehd'
                },
                {
                    'key': 'Version',
                    'value': '4'
                }
            ],
            requiresCompatibilities=[
                'FARGATE'
            ],
            cpu='512',
            memory='1024',
            runtimePlatform={
                'cpuArchitecture': 'X86_64',
                'operatingSystemFamily': 'LINUX'
            }
        )

    def invoke_Create_EFS(self, name_srv):
        response = self.Lambda_client.invoke(
            FunctionName='arn:aws:lambda:us-east-1:744663755802:function:CreateEFSDirectory',
            InvocationType='RequestResponse',
            Payload=json.dumps({'name': name_srv}),
        )

        response_payload = json.loads(response['Payload'].read().decode('utf-8'))
        return response_payload.get('statusCode')

    def create_service(self, service_name, taskDefintionArn):
        return self.ECS_client.create_service(
            cluster=self.cluster,
            serviceName=service_name,
            taskDefinition=taskDefintionArn,
            desiredCount=1,
            launchType='FARGATE',
            networkConfiguration={
                'awsvpcConfiguration': {
                    'subnets': [
                        'subnet-016cdd75caabfde09',
                        'subnet-0f47a165354326830',
                    ],
                    'securityGroups': [
                        'sg-00d571ca440dd9acf',
                    ],
                    'assignPublicIp': 'ENABLED'
                }
            },
        ).get('service', {}).get('serviceArn', None)

    def server_info(self, serviceArn, isMinecraft=True):
        # List tasks for the given service
        tasks_response = self.ECS_client.list_tasks(
            cluster=self.cluster,
            serviceName=serviceArn
        )

        if not tasks_response['taskArns']:
            print("No tasks found for the service.")
            return "Stopped", None, None

        # Describe the first task in the list
        task_arn = tasks_response['taskArns'][0]
        describe_tasks_response = self.ECS_client.describe_tasks(
            cluster=self.cluster,
            tasks=[task_arn]
        )

        if not describe_tasks_response['tasks']:
            print("No task details found.")
            if isMinecraft:
                return None, None, None
            else:
                return False

        # Get the task status
        task_status = describe_tasks_response['tasks'][0]['lastStatus']

        if task_status != "RUNNING":
            if isMinecraft:
                return task_status, None, None
            else:
                return False

        # Find the ENI ID
        attachments = describe_tasks_response['tasks'][0]['attachments']
        eni_id = None
        for attachment in attachments:
            if attachment['type'] == 'ElasticNetworkInterface':
                for detail in attachment['details']:
                    if detail['name'] == 'networkInterfaceId':
                        eni_id = detail['value']
                        break

        if not eni_id:
            print("No ENI found for the task.")
            if isMinecraft:
                return task_status, None, None
            else:
                return False

        # Describe the network interface to get the public IP
        describe_eni_response = self.EC2_client.describe_network_interfaces(
            NetworkInterfaceIds=[eni_id]
        )

        public_ip = describe_eni_response['NetworkInterfaces'][0].get('Association', {}).get('PublicIp')

        if isMinecraft:
            try:
                self.whitelist_list(public_ip)
            except Exception:
                return "Preparing World", None, None

            server_address = self.get_Server_Address(public_ip)
            if server_address:
                return task_status, public_ip, server_address
            server_address = self.post_ip(public_ip)

            return task_status, public_ip, server_address
        return public_ip

    def stop_server(self, serviceArn, desired_count):
        if desired_count == 0:
            public_ip = self.server_info(serviceArn, False)
            if public_ip:
                if not self.delete_ip(public_ip):
                    print("didn't work")
                    return False

        response = self.ECS_client.update_service(
            cluster=self.cluster,
            service=serviceArn,
            desiredCount=desired_count
        )

        if response.get('service').get('desiredCount') == desired_count:
            return True
        else:
            return False

    def whitelist_add(self, server_ip, usernames):
        with Client(server_ip, 25575, passwd=self.recon_password) as mcr:
            for username in usernames:
                mcr.run(f"whitelist add {username}")
        return True

    def whitelist_remove(self, server_ip, usernames):
        with Client(server_ip, 25575, passwd=self.recon_password) as mcr:
            for username in usernames:
                mcr.run(f"whitelist remove {username}")
        return True

    def whitelist_list(self, server_ip):
        with Client(server_ip, 25575, passwd=self.recon_password) as client:
            response = client.run('whitelist list')

        print(response)
        return response

    def get_Server_Address(self, server_ip, router=None):
        if router is None:
            router = self.server_info(self.arn_mcRouter, False)
        if not router:
            print("Hell Nahh")
            return False

        url = f"http://{router}:25564/routes"

        headers = {"Accept": "application/json"}
        response = requests.get(url, headers=headers)

        if response.status_code != 200:
            print(response.status_code)
            return False

        dict_res = json.loads(response.text)
        server_address = None
        try:
            server_address = list(dict_res.keys())[list(dict_res.values()).index(server_ip + ":25565")]
        except Exception:
            server_address = None


        return server_address

    def post_ip(self, server_ip):
        router = self.server_info(self.arn_mcRouter, False)
        if not router:
            return False
        url = f"http://{router}:25564/routes"

        server_address = random.choice(self.domain_names)
        headers = {"Content-Type": "application/json"}
        data = {
            "serverAddress": server_address,
            "backend": server_ip + ":25565"
        }

        response = requests.post(url, json=data, headers=headers)

        if response.status_code == 201:
            return server_address
        else:
            return None

    def delete_ip(self, server_ip):
        router = self.server_info(self.arn_mcRouter, False)
        if not router:
            return False

        server_address = self.get_Server_Address(server_ip, router)
        if server_address is None:
            return True

        url = f"http://{router}:25564/routes"
        response = requests.delete(url + f"/{server_address}")
        if response.status_code == 200:
            return True
        else:
            return False

    def upload_files(self, directory, s3_path):
        for root, _, files in os.walk(directory):
            for file in files:
                file_path = os.path.join(root, file)
                s3_key = os.path.join(s3_path, os.path.relpath(file_path, directory))

                try:
                    self.s3_client.upload_file(file_path, bucket, s3_key)
                    print(f'Successfully uploaded {file_path} to s3://{self.s3_bucket}/{s3_key}')
                except FileNotFoundError:
                    return False
                except NoCredentialsError:
                    return False
