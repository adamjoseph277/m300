from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin

db = SQLAlchemy()

class Users(UserMixin, db.Model):
    __tablename__ = "Users"

    name_user = db.Column(db.String(45), primary_key=True)
    email_user = db.Column(db.String(45), nullable=False)
    hash_user = db.Column(db.String(60), nullable=False)

    def get_id(self):
        return self.name_user
    
class Maintainer_Roles(db.Model):
    __tablename__ = "Maintainer_Roles"

    id_role = db.Column(db.Integer, primary_key=True)
    name_role = db.Column(db.String(45), nullable=False)


class Mcsrvs(db.Model):
    __tablename__ = "Mcsrvs"

    id_mcsrv = db.Column(db.Integer, primary_key=True)
    arn_taskdef_mcsrv = db.Column(db.String(255))
    arn_service_mcsrv = db.Column(db.String(255))
    name_mcsrv = db.Column(db.String(45), nullable=False)


class User_has_Mcsrv(db.Model):
    __tablename__ = "User_has_Mcsrv"

    name_user = db.Column(db.String(45), db.ForeignKey('Users.name_user'), primary_key=True)
    id_mcsrv = db.Column(db.Integer, db.ForeignKey('Mcsrvs.id_mcsrv'), primary_key=True)
    id_role = db.Column(db.Integer, db.ForeignKey('Maintainer_Roles.id_role'), nullable=False)


class Backups(db.Model):
    __tablename__ = "Backups"

    id_backup = db.Column(db.Integer, primary_key=True)
    name_backup = db.Column(db.String(45), nullable=False)
    path_s3 = db.Column(db.String(45), nullable=False)
    id_mcsrv = db.Column(db.Integer, db.ForeignKey('Mcsrvs.id_mcsrv'), primary_key=True)
