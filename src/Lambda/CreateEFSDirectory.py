import os

def lambda_handler(event, context):
    name = event.get('name', None)
    # EFS mount point configured in the Lambda file system settings
    efs_mount_point = '/mnt/efs'

    # Directory to create
    directory_path = os.path.join(efs_mount_point, name)

    try:
        if not os.path.exists(directory_path) and name:
            os.makedirs(directory_path)
            return {
                'statusCode': 201,
                'body': f"Directory '{directory_path}' created successfully."
            }
        else:
            return {
                'statusCode': 200,
                'body': f"Directory '{directory_path}' already exists."
            }
    except Exception as e:
        return {
            'statusCode': 500,
            'body': f"Error creating directory '{directory_path}': {str(e)}"
        }
