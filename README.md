# Joseph - m300 Projekt

## Demonstration

The Demonstration Video für dieses Projekt findet man [hier](https://1drv.ms/v/s!ApHdMcHuKzQ5gddVTLqUVHN6_Kv_bw?e=Vxs5aD)
## Systemdokumentation

---

## 1 Projektbeschreibung
### 1.1 Überblick

Diese Webanwendung ermöglicht es Benutzern, Minecraft-Server zu erstellen, zu verwalten und zu sichern. Sie bietet eine Benutzeroberfläche, über die verschiedene Aktionen wie Starten, Stoppen und Backupen von Servern sowie das Whitelisten anderer Benutzer ermöglicht wird. Die Anwendung nutzt im Hintergrund einen Reverse Proxy, um die Minecraft-Server an Domainnamen zu binden.

### 1.2 Benutzerverwaltung

- Benutzer  können sich registrieren und mit ihren Anmeldedaten in die Webanwendung einloggen.
- Die Authentifizierung stellt sicher, dass nur autorisierte Benutzer auf die Verwaltungsfunktionen zugreifen können.


### 1.3 Serververwaltung

- Benutzer können neue Minecraftserver erstellen. Dies umfasst die Konfiguration der Servereinstellungen wie Servername, Spielmodus, maximale Spieleranzahl usw.
- Benutzer können ihre erstellten Minecraft-Server über die Weboberfläche starten.
- Benutzer können ihre laufenden Minecraft-Server stoppen
- Benutzer önnen manuelle Backups ihrer Server erstellen, um Datenverlust vorzubeugen.
- Benutzer können andere Spieler zur Whitelist hinzufügen oder entfernen, um den Zugriff auf ihren Minecraft-Server zu Steuren

### 1.4 Reverseproxy
- Der Reverse Proxy

## 2. Umgebung
### 2.1 Überblick

Die Infrastruktur, wurde in einem AWS-Learnerlab aufgebaut.
In Learner Lab gelten andere Reglen als auf einem regulären AWS-Account
[Quelle 1](https://labs.vocareum.com/web/2451565/2932819.0/ASNLIB/public/docs/lang/en-us/README.html?vockey=b526cc5008bf74f6ce33f6f5816e8c5dc080d575cbd453769cb2d55cfff4601e#envOverview)

### 2.2 AWS-Infrastruktur

Die AWS Infrastruktur wurde so aufgebaut:

![AWS-Infrasturcutre](./img/aws-infrastructure.png)

Hier sieht man alle Services welche Benutzt werden, diese werden in dieser Dokumentation noch besser beschrieben.


### 2.3 Netzwerk
### 2.3.1 Netzwerkplan

In diesem Projekt macht ein Netzwerkplan nicht so viel Sinn, weil jede IP-Adresse dynamisch ist deshalb habe ich nur die Umbegungs diagramm erstellt.
### *2.3.2 Virtual private cloud*
Ein VPC ist ein konfigurierbarer Pool von geteilten resourcen, welches in der AWS ein Level von Isolation bietet. [Quelle 2](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html)

Ein VPC mit dem Namen **pixelcube-vpc** wurde in der Region **us-east-1** erstellt.
Die einzigen weiteren Konfiguration welche wichtig sind für das Projekt ist, dass die Cidr: 10.0.0.0/16 ist und das ein Internetgateway angefügt wurde, damit die public-subnets Internet haben.

### *2.3.3 Subnets*
Ein Subnet ist ein Bereich von IP-Adressen in einem VPC [Qulle 3](https://docs.aws.amazon.com/de_de/vpc/latest/userguide/what-is-amazon-vpc.html#:~:text=Ein%20Subnetz%20ist%20ein%20Bereich,IP%2DAdressierung)

Im VPC **pixelcube-vpc** wurden 4 Subnetze erstellt, 2 public und 2 private. je ein public und private subnets sind über zwei Availibility Zonen gestreckt. 
Ausserdem wurde noch ein weiteres Subnet erstellt, für die Minecraftserver. Das strecken über mehrere AZ sorgt dafür das auch beim Ausfall einer Zone die Umgebung nicht kompromiert wird.

**pixelcube-subnet-public1,2:**

Die public subnets sind für die Flask Applikationen gedacht welche dann über diese zwei gestreckt werden.

**pixelcube-subnet-private1,2:**

Die Private Subnets sind da für die Datenbank Instanzen, weil auf diese braucht man von aussen kein zugriff, deshalb kann man nur VPC auf den zugreifen.



### *2.3.4 Security Groups*
Security groups, dienen als virtuaelle Firewalls für jegliche AWS Recourssen und Services, welches einem erlaubt den Inbound und Outbound Netzwerkverkehr zu steuren. [Quelle 4](https://www.checkpoint.com/cyber-hub/cloud-security/what-is-aws-security-groups/#:~:text=An%20AWS%20security%20group%20acts,traffic%20from%20your%20instance%2C%20respectively.)

Die Sicherheitsgruppen welche erstellt wurden, für diese Umgebung sind wie folgt.

| SG        | Inbound Rules                           | Source    | Outbound Rules | Description                                            |
|-----------|-----------------------------------------|-----------|----------------|--------------------------------------------------------|
| Test      | AAT                                     | Everyone  | AAT            | Zum Testen und development                             |
| Flask     | Allow Port 80,443, 2049, 3306 und 25575 | Everyone  | AAT            | Für kommunikation mit Datenbank, Lambda, RCON und User |
| Lambda    | Allow Port 80, 443 und 2049             | Flask     | AAT            | Für Kommunikation mit Flaskapp und EFS-SHare           |
| MC-Router | Allow Port 25564, 25565                 | Everyone  | AAT            | Für Kommunikation mit MC-Server und API-requests       |
| MC-Server | Allow Port 25565, 25575                 | MC-Router | AAT            | Für Kommunikation mit Spieler und Rcon                 |
| RDS       | Allow Port 3306                         | Flask     | AAT            | Für Kommunikation mit Flask DB-Verbindung              |
| ALB       | Allow Port 80                           | Everyone  | AAT            | Für Kommunikation mit User und Flask app               |


**AAT = Allow all Traffic*

Diese Security Groups stellen sicher das die Services nur die nötigsten Firewallrules für das Netzwerk haben.

## 3. Flask Applikation
In diesem Kapitel wird die Flask Applikation beschrieben.

### 3.1 Routen
Das sind die Routen welche erstellet wurden für dieses Projekt.

| Route                                | Methoden  | Authentifiezierung | Authorisierung | Beschreibung                                                                                                                                                                                                                               |
|--------------------------------------|-----------|--------------------|----------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| /register                            | GET, POST | Nein               | Nein           | - Neue User können sich Registrieren<br/>- Cheked ob der User schon existiert. <br/> - Hashed das Passwort mit bycrpyt bevor es in die Datenbank gespeichert wird<br/> - Logged den User automatisch ein nach dem man sich registriert hat |
| /login                               | GET, POST | Nein               | Nein           | - User können sich einloggen<br/>- Verfiziert das Passwort mit bycrpt<br/>- Leitet den User auf die Homepage weiter                                                                                                                        |
| /logout                              | GET       | Nein               | Nein           | - Loggt den User aus und leitet den auf die Login Seite weiter                                                                                                                                                                             |
| /                                    | GET       | Ja                 | Nein           | - Zeigt die Homepage an mit allen Servern welche dem User gehören                                                                                                                                                                          |
| /create_server                       | GET, POST | Ja                 | Nein           | - Erlaubt den User einen Minecraft Server zu erstellen<br/>- Registriert eine neue Task definiton für AWS ECS<br/>- Erstellt ein EFS Volume für den Server<br/>- fügt die Information des Servers in die Datenbank                         |
| /edit_server/<server_id>             | GET, POST | Ja                 | Ja             | - Erlaubt Server besitzer die Whitelist zu editieren<br/>- Bezieht die IP und den Status des Servers durch ECS<br/> - Updated die Server Informatioenn in der Datenbank                                                                    |
| /server/<server_id>                  | GET       | Ja                 | Ja             | - Bezieht deteilierte Infromationen des Servers wie IP und Status                                                                                                                                                                          |
| /server/<server_id>/start            | POST      | Ja                 | Ja             | - Started den Server in dem er den "desired count in AWS ECS updated"                                                                                                                                                                      |
| /server/<server_id>/stop             | POST      | Ja                 | Ja             | - Stopped den Server in dem er den "desired count in AWS ECS updated"                                                                                                                                                                      |
| /server/<server_id>/whitelist/add    | POST      | Ja                 | Ja             | - fügt User zur Whitelist mit dem RCON Protokoll                                                                                                                                                                                           |
| /server/<server_id>/whitelist/remove | POST      | Ja                 | Ja             | - entfernt User von der Whitelist mit dem RCON Protokoll                                                                                                                                                                                   |
| /server/<server_id/backup            | POST      | Ja                 | Ja             | - Backed die Server Daten auf einen S3 Bucket up<br/>- Speichert die Daten in der Datenbank                                                                                                                                                |
| /server/<server_id/restore           | POST      | Ja                 | Ja             | - Restored die Daten von S3 wieder auf das Server Volume q                                                                                                                                                                                 |


### 3.2 Backups
Die Webappliktion gibt dem User die Möglichkeit, den jetzigen Stand des Minecraftservers Backzuupen.
Das Backup funktioniert so indem man die FIles aus dem EFS Share nimmt eines Minecraft servers und diese dann in ein S3 Bucket moved.

Die beste Lösung wäre mit Datasync. Mit Datasync kann man die Daten einfach von einer Location zur anderen moven [Quelle 5](https://aws.amazon.com/datasync/faqs/#:~:text=A%3A%20AWS%20DataSync%20is%20an,providers%2C%20and%20AWS%20Storage%20services.). Diese Option ging für mich aber nicht, weil ich vom AWS Learner LAB aus keinen Zugriff auf diesen Service habe. deshalb musste ich mit meiner eigenen Lösung auf kommen.

Die Lösung die dann Benutzt wurde, ist, man mounted das EFS Directory im /mnt/efs des Webservers und kann so auf die Daten zugreifen. Es ist zwar nicht die eleganteste Methode aber die einzige welche ich umsetzen kann welche nicht zu umständlich wird.


### 3.3 Login

Ein Authentifizierungssystem wurde überarbetiet, sodass sich der User nur seine eigenen Server anschauen und editieren kann.

![Login](img/Login.png)
![Register](img/Register.png)
### 3.4 Dashboard

Das Dashboard zeigt alle Server an welche vom User besitzt werden, dort kann man dann auf die Page für die Server Info oder zum Server editieriern.

![Dashboard](./img/Dashboard.png)

### 3.5 Server_info

Die Server Info seite die Informationen IP und Hostnamens eines Servers. Ausserdem kann der User den Server stoppen und Starten.
Den Server kann unteranderm auch Backupen und Restoren. Beim Backup wird das EFS Volume in den Backup server gemoved. beim Restore wird das gegenteil gemacht

**S3 Backup-Folder**

![Backup Folder](/img/S3-Folder.png)
  

### 3.6 Edit_Server

Hier kann man User von der Whitelist hinzufügen und entfernen .

![Whitelist](img/Whitelist.png)

## 4. Services

### 4.1 AWS ECS
AWS ECS ist ein Full gemanagetes COntainer Service von AWS [Quelle 6](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html)


#### 4.1.1 MC-Servers
Alle MC-Server haben die gleichen Einstellungen und die Gleiche Taskdefinition, das Json sieht so aus.

```
{
    "family": "demo-demo",
    "containerDefinitions": [
        {
            "name": "test_mc",
            "image": "itzg/minecraft-server",
            "cpu": 0,
            "portMappings": [
                {
                    "containerPort": 25565,
                    "hostPort": 25565,
                    "protocol": "tcp"
                },
                {
                    "containerPort": 25575,
                    "hostPort": 25575,
                    "protocol": "tcp"
                }
            ],
            "essential": true,
            "environment": [
                {
                    "name": "RCON_PASSWORD",
                    "value": "adufaudfdjaieiafidsfi"
                },
                {
                    "name": "EULA",
                    "value": "TRUE"
                },
                {
                    "name": "ENABLE_WHITELIST",
                    "value": "TRUE"
                }
            ],
            "mountPoints": [
                {
                    "sourceVolume": "data-vol",
                    "containerPath": "/data",
                    "readOnly": false
                }
            ],
            "volumesFrom": [],
            "logConfiguration": {
                "logDriver": "awslogs",
                "options": {
                    "awslogs-group": "/ecs/testtask",
                    "awslogs-create-group": "true",
                    "awslogs-region": "us-east-1",
                    "awslogs-stream-prefix": "ecs"
                },
                "secretOptions": []
            },
            "systemControls": []
        }
    ],
    "taskRoleArn": "arn:aws:iam::744663755802:role/LabRole",
    "executionRoleArn": "arn:aws:iam::744663755802:role/LabRole",
    "networkMode": "awsvpc",
    "volumes": [
        {
            "name": "data-vol",
            "efsVolumeConfiguration": {
                "fileSystemId": "fs-00bf09c9cdf51e73a",
                "rootDirectory": "/demo",
                "transitEncryption": "ENABLED",
                "transitEncryptionPort": 2049
            }
        }
    ],
    "requiresCompatibilities": [
        "FARGATE"
    ],
    "cpu": "512",
    "memory": "1024",
    "runtimePlatform": {
        "cpuArchitecture": "X86_64",
        "operatingSystemFamily": "LINUX"
    },
    "tags": [
        {
            "key": "Version",
            "value": "4"
        },
        {
            "key": "Owner",
            "value": "Onkeljoehd"
        }
    ]
}
```

Hier erkennt man wie das RCON Protokoll gemappt wird.
RCON ist ein Protokoll von wo man eine Fernwartung machen kann [Quelle 7](https://en.wikipedia.org/wiki/Remote_administration)

Ausserdem wird die Whitelist enabled [Quelle 8](https://www.g-portal.com/wiki/de/minecraft-server-whitelist-einrichten/#:~:text=Server%3A%20Whitelist%20einrichten-,Minecraft%20Whitelist,du%20normalerweise%20deine%20Serverkonfigurationsdateien%20bearbeiten.)

#### 4.2.2 MC-Router
Der MC-Router ist der Service mit welchem man die Domain Namen auf die jeweiligen Container zu mappen.
Dieses mach man mit einer API aber man kann hier  mehr dazu lesen [Qulle x](https://github.com/itzg/docker-minecraft-server)

Es hat 20 Namen welche zur Verfügung stehn um den Namen zu mappen und die wären:
```
            "java.pixelcube.ch",
            "sumatra.pixelcube.ch",
            "borneo.pixelcube.ch",
            "sulawesi.pixelcube.ch",
            "luzon.pixelcube.ch",
            "taiwan.pixelcube.ch",
            "honshu.pixelcube.ch",
            "hainan.pixelcube.ch",
            "cuba.pixelcube.ch",
            "madagascar.pixelcube.ch",
            "sicily.pixelcube.ch",
            "sriLanka.pixelcube.ch",
            "cyprus.pixelcube.ch",
            "crete.pixelcube.ch",
            "corsica.pixelcube.ch",
            "iceland.pixelcube.ch",
            "newfoundland.pixelcube.ch",
            "tasmania.pixelcube.ch",
            "mauritius.pixelcube.ch",
            "malta.pixelcube.ch"
```

Das Json des MC Routers sieht so aus:

```
{
    "family": "mc-router",
    "containerDefinitions": [
        {
            "name": "mc-router",
            "image": "itzg/mc-router",
            "cpu": 0,
            "portMappings": [
                {
                    "name": "minecraft",
                    "containerPort": 25565,
                    "hostPort": 25565,
                    "protocol": "tcp",
                    "appProtocol": "http"
                },
                {
                    "name": "api",
                    "containerPort": 25564,
                    "hostPort": 25564,
                    "protocol": "tcp",
                    "appProtocol": "http"
                }
            ],
            "essential": true,
            "environment": [
                {
                    "name": "API_BINDING",
                    "value": ":25564"
                }
            ],
            "environmentFiles": [],
            "mountPoints": [],
            "volumesFrom": [],
            "ulimits": [],
            "logConfiguration": {
                "logDriver": "awslogs",
                "options": {
                    "awslogs-group": "/ecs/mc-router",
                    "awslogs-create-group": "true",
                    "awslogs-region": "us-east-1",
                    "awslogs-stream-prefix": "ecs"
                },
                "secretOptions": []
            },
            "systemControls": []
        }
    ],
    "taskRoleArn": "arn:aws:iam::744663755802:role/LabRole",
    "executionRoleArn": "arn:aws:iam::744663755802:role/LabRole",
    "networkMode": "awsvpc",
    "requiresCompatibilities": [
        "FARGATE"
    ],
    "cpu": "1024",
    "memory": "3072",
    "runtimePlatform": {
        "cpuArchitecture": "X86_64",
        "operatingSystemFamily": "LINUX"
    }
}
```

Hier erkennt man nur Speziel das die API auf 25564 gebunden ist das man auf diese zugreifen kann.

### 4.3 EC2 Instanz
AWS EC2 ist ein Service welcher Virtuelle Maschienen am User provided [Quelle 9](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html)

Auf den EC2 Instanzen läuft die Vorher erwähnte Flask app.

### 4.5 Launch Template

- Enthält die Konfiguration für die EC2-Instanzen.
- Sicherstellt, dass die Flask App auf Port 8000 läuft.

### 4.6 Target Group
Eine Target Group ist eine Gruppe von Ressourcen, auf die ein Load Balancer Anfragen verteilen kann. Diese Ressourcen können EC2-Instanzen, IP-Adressen oder Lambda-Funktionen sein. Die Target Group ermöglicht eine flexible Verteilung des Traffics basierend auf verschiedenen Kriterien wie Instanz-Status und Health-Checks.[]()

- Lauscht auf Port 8000.
- Enthält die EC2-Instanzen, die über das Launch Template gestartet werden.
- Führt Health Checks durch, um die Verfügbarkeit der Instanzen sicherzustellen.

#### 4.7 Application Load Balancer (ALB)
- Lauscht auf Port 80.
- Leitet eingehenden Traffic an die Target Group weiter, die auf Port 8000 lauscht.
- Führt Health Checks durch, um sicherzustellen, dass der Traffic nur an gesunde Instanzen weitergeleitet wird.

### 4.8 Autoscaling Group
- Verwendet das Launch Template, um EC2-Instanzen zu starten.
- Skaliert automatisch zwischen 2 und 4 Instanzen basierend auf definierten Skalierungsrichtlinien.

### 4.9 Diagramm der Architektur

```
          +---------------------+
          |  Application Load   |
          |      Balancer       |
          |      (Port 80)      |
          +---------+-----------+
                    |
                    v
          +---------------------+
          |    Target Group     |
          |     (Port 8000)     |
          +---------+-----------+
                    |
          +---------------------+
          |   Autoscaling Group |
          |     2 - 4 EC2       |
          |    Instances (LT)   |
          +---------------------+
 ```


### 4.10 AWS Relational Database Service (RDS)

AWS RDS ist ein von AWS gemangtetr Webserver welches einem erlaubt Datenbanken zu erstellen betreiben und warten [Quelle 10](https://docs.aws.amazon.com/de_de/AmazonRDS/latest/UserGuide/Welcome.html)

Ich habe ein Datenbankserver im 

### 4.11 AWS Elastic File System (EFS)
AWS EFS ist ein skalierbarer, voll gemanageted File Spicher Services, welche auf mehreren Endgeräten gemounted werden kann, sommit hat man einen geteilten access. Ähnliche Produkte ausserhalb AWS sind NFS oder Samba. [Quelle 13](https://docs.aws.amazon.com/efs/latest/ug/whatisefs.html)

Ich habe ein EFS-File Share erstellt für 


### 4.12 AWS Lambda
AWS Lambda ist ein serverloser computing service, welches einem erlaubt Code zu executieren ohne Server zu erstellen oder managen. [Quelle 11](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html)
### *4.12.1 CreateEFSDirectory*

CreteEFSDirectory ist eine Lambda Funktion welche es mir erlaubt, ein Directory im EFS Share zu erstellen, damit ich diesen für die Conatiner Volumes benutzen kann.

Den Code findet man [hier](./src/Lambda/CreateEFSDirectory.py)

diese Lambda Funktion wurde im pixelcube VPC abgelegt und bekam die Security Group Lambda zugefügt.

Damit die Lambda Funktion auf das EFS zugreifen kann wurde noch ein Filesystem hinzugefügt. der EFS Share wurde auf den Mount path `/mnt/efs` gemappet. Im Code sieht man auch wie diese benutzt wird.


### 4.13 AWS S3
AWS S3 ist eine Storage Lösung von AWS 

Ich habe einem Bucket welche meine Backups für die MC Server speichert. dieser Bucket hat die Berechtigungen das man mit der Labrole auf die Daten zugreifen kann aber sonst nicht.

## 5. Monitoring
Dieses Kapitel beschreibt das Monitoring-Setup für eine AWS-Umgebung, die hauptsächlich Minecraft-Container verwendet. Das Ziel ist es, die CPU-Auslastung der Instanzen zu überwachen und automatische Skalierung sicherzustellen.

### 5.1 Infrastrukturkomponenten
- **Amazon EC2**: Bereitstellung von Instanzen für die Minecraft-Container.
- **Amazon ECS (Elastic Container Service)**: Verwaltung der Container.
- **Amazon Auto Scaling**: Dynamisches Anpassen der Anzahl von Instanzen basierend auf der CPU-Auslastung.
- **Amazon CloudWatch**: Überwachung und Protokollierung der Leistungsmetriken.

### 5.2 Auto Scaling Konfiguration
Auto Scaling ermöglicht die automatische Skalierung der Anzahl von EC2-Instanzen basierend auf der CPU-Auslastung.

### 5.2.1 Auto Scaling Gruppen
- **Name**: Minecraft-AutoScaling-Group
- **Minimum Anzahl von Instanzen**: 2
- **Maximum Anzahl von Instanzen**: 4
- **Zielgruppenrichtlinie**: Skalierung basierend auf der CPU-Auslastung

### 5.3 Skalierungsrichtlinien
- **Auslöser**: CPU-Auslastung
- **Schwellenwert**: 80% CPU-Auslastung
- **Maßnahmen**: 
  - Skalieren Sie nach oben: Fügen Sie eine zusätzliche Instanz hinzu, wenn die durchschnittliche CPU-Auslastung über 80% liegt.
  - Skalieren Sie nach unten: Entfernen Sie eine Instanz, wenn die durchschnittliche CPU-Auslastung unter 50% liegt.

## 5.4 CloudWatch Monitoring
Amazon CloudWatch wird verwendet, um die Leistungsmetriken der Container und Instanzen zu überwachen.

### 5.4.1 Metriken
- **CPU-Auslastung**
- **Speicherauslastung**
- **Netzwerkverkehr**
- **Anzahl der aktiven Instanzen**


### 5.5 Container Management
Alle Minecraft-Container werden innerhalb von Amazon ECS verwaltet. Die CloudWatch-Agenten sind in die Container integriert, um Metriken direkt zu CloudWatch zu senden.

### 5.5.1 ECS Cluster
- **Name**: Minecraft-ECS-Cluster
- **Container Name**: Minecraft-Container

### 5.5.2 Task Definition
- **Name**: MinecraftTaskDefinition
- **CPU**: 1024 (1 vCPU)
- **Memory**: 2048 MiB
- **Container Port**: 25565


### 5.6 Sicherheitsüberlegungen
- **IAM Rollen**: Spezifische IAM-Rollen für ECS und EC2 Instanzen, um sicherzustellen, dass nur die notwendigen Berechtigungen vergeben werden.
- **VPC und Subnetze**: Nutzung von privaten Subnetzen für EC2 Instanzen, um die Sicherheit der Minecraft-Server zu erhöhen.


## 6. Testing
## 6. Testing

### 6.1 Testumgebung

### 6.2 Testfälle

#### 6.2.1 Benutzerregistrierung und -authentifizierung

1. **Registrierung:**
   - **Test:** Registrierung eines neuen Benutzers.
   - **Erwartetes Ergebnis:** Benutzer wird erfolgreich registriert und automatisch eingeloggt.
   - **Ergebnis:** Erfolgreich.

2. **Login:**
   - **Test:** Einloggen mit gültigen und ungültigen Anmeldedaten.
   - **Erwartetes Ergebnis:** Erfolgreiches Einloggen mit gültigen Anmeldedaten, Fehlermeldung bei ungültigen Anmeldedaten.
   - **Ergebnis:** Erfolgreich.

#### 6.2.2 Serververwaltung

1. **Server erstellen:**
   - **Test:** Erstellen eines neuen Minecraft-Servers.
   - **Erwartetes Ergebnis:** Server wird erfolgreich erstellt und in der Datenbank gespeichert.
   - **Ergebnis:** Erfolgreich.

2. **Server starten:**
   - **Test:** Starten eines erstellten Servers.
   - **Erwartetes Ergebnis:** Server wird gestartet und ist erreichbar.
   - **Ergebnis:** Erfolgreich.

3. **Server stoppen:**
   - **Test:** Stoppen eines laufenden Servers.
   - **Erwartetes Ergebnis:** Server wird gestoppt.
   - **Ergebnis:** Erfolgreich.

4. **Whitelist verwalten:**
   - **Test:** Benutzer zur Whitelist hinzufügen und entfernen.
   - **Erwartetes Ergebnis:** Benutzer werden erfolgreich zur Whitelist hinzugefügt oder entfernt.
   - **Ergebnis:** Erfolgreich.

#### 6.2.3 Backup und Wiederherstellung

1. **Backup erstellen:**
   - **Test:** Manuelles Backup eines laufenden Servers.
   - **Erwartetes Ergebnis:** Serverdaten werden erfolgreich in S3 gespeichert.
   - **Ergebnis:** Erfolgreich.

2. **Backup wiederherstellen:**
   - **Test:** Wiederherstellen eines Servers aus einem Backup.
   - **Erwartetes Ergebnis:** Serverdaten werden erfolgreich von S3 wiederhergestellt.
   - **Ergebnis:** Erfolgreich.

#### 6.2.4 Performance- und Belastungstests

1. **Lasttest:**
   - **Test:** Simulieren von hoher Last auf einem Minecraft-Server.
   - **Erwartetes Ergebnis:** Server bleibt stabil, Auto-Scaling funktioniert korrekt.
   - **Ergebnis:** Erfolgreich.

2. **Monitoring:**
   - **Test:** Überwachen der Serverleistung mit CloudWatch.
   - **Erwartetes Ergebnis:** Korrekte Anzeige von CPU-, Speicher- und Netzwerkmetriken.
   - **Ergebnis:** Erfolgreich.

### 6.3 Fehlerbehebung

1. **Fehlerprotokollierung:**
   - AWS CloudWatch Logs wurden zur Fehlerprotokollierung verwendet.
   - Jedes auftretende Problem wurde dokumentiert und behoben.

2. **Bugfixes:**
   - Identifizierte Fehler wurden behoben und die betroffenen Testfälle erneut ausgeführt.

### 6.4 Abschlusstests

- Nach Abschluss aller Testfälle wurden Abschlusstests durchgeführt, um die Gesamtfunktionalität und Stabilität des Systems sicherzustellen.
- Alle Tests wurden erfolgreich abgeschlossen, und die Webanwendung wurde als einsatzbereit eingestuft.

## 7. Reflexion
Dieses Projekt hat mir viel Spass gemacht, weil ich früher ein Minecrafter gewesen war. auch wenn ich viele Probleme hatte konnte ich sie lösen oder Workarounds finden. Ich habe Schlussendlich über mich herausgefunden das ich Dokumentationen nicht kurzfristig machen sollte sondern etwas Zeit für diese einplanen sollte um nicht in Stress zu geraten. Ich habe viel gelernt vorallem im Bereich Boto3 und dem AWS Command line. dies war spannend weich ich öfters schon mit der Console bei AWS gearbeitet habe aber nie mit CLI und Ich glaube das ist das erste mal was ich richtig in einem LearnerLab hinbekommen habe. 

## 8. Quellenverzeichnis

1. [AWS Learner Lab](https://labs.vocareum.com/web/2451565/2932819.0/ASNLIB/public/docs/lang/en-us/README.html?vockey=b526cc5008bf74f6ce33f6f5816e8c5dc080d575cbd453769cb2d55cfff4601e#envOverview)
2. [Amazon Virtual Private Cloud (VPC)](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html)
3. [Amazon VPC Subnets](https://docs.aws.amazon.com/de_de/vpc/latest/userguide/what-is-amazon-vpc.html#:~:text=Ein%20Subnetz%20ist%20ein%20Bereich,IP%2DAdressierung)
4. [AWS Security Groups](https://www.checkpoint.com/cyber-hub/cloud-security/what-is-aws-security-groups/#:~:text=An%20AWS%20security%20group%20acts,traffic%20from%20your%20instance%2C%20respectively.)
5. [AWS DataSync](https://aws.amazon.com/datasync/faqs/#:~:text=A%3A%20AWS%20DataSync%20is%20an,providers%2C%20and%20AWS%20Storage%20services.)
6. [AWS ECS (Elastic Container Service)](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html)
7. [Remote administration (RCON)](https://en.wikipedia.org/wiki/Remote_administration)
8. [Minecraft Server Whitelist Einrichten](https://www.g-portal.com/wiki/de/minecraft-server-whitelist-einrichten/#:~:text=Server%3A%20Whitelist%20einrichten-,Minecraft%20Whitelist,du%20normalerweise%20deine%20Serverkonfigurationsdateien%20bearbeiten.)
9. [Amazon EC2](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html)
10. [Amazon RDS (Relational Database Service)](https://docs.aws.amazon.com/de_de/AmazonRDS/latest/UserGuide/Welcome.html)
11. [AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html)
12. [AWS EFS (Elastic File System)](https://docs.aws.amazon.com/efs/latest/ug/whatisefs.html)
